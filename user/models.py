from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='profile')
    company = models.TextField(max_length=50, blank=True)
    phone_number = models.TextField(max_length=50, blank=True)
    street_address = models.TextField(max_length=50, blank=True)
    city = models.TextField(max_length=50, blank=True)
    state = models.TextField(max_length=50, blank=True)
    postal_code = models.TextField(max_length=50, blank=True)
    country = models.TextField(max_length=50, blank=True)
    categories = models.ManyToManyField('projects.ProjectCategory', related_name='competance_categories')

    def __str__(self):
        return self.user.username


    def get_user_task_permissions(self, task):
        if self.user == task.project.user.user:
            return {
                'write': True,
                'read': True,
                'modify': True,
                'owner': True,
                'upload': True,
            }
        if task.accepted_task_offer() and task.accepted_task_offer().offerer == self:
            return {
                'write': True,
                'read': True,
                'modify': True,
                'owner': False,
                'upload': True,
            }
        user_permissions = {
            'write': False,
            'read': False,
            'modify': False,
            'owner': False,
            'view_task': False,
            'upload': False,
        }
        user_permissions['read'] = user_permissions['read'] or self.task_participants_read.filter(id=task.id).exists()

        # Team members can view its teams tasks
        user_permissions['upload'] = user_permissions['upload'] or self.teams.filter(task__id=task.id, write=True).exists()
        user_permissions['view_task'] = user_permissions['view_task'] or self.teams.filter(task__id=task.id).exists()

        user_permissions['write'] = user_permissions['write'] or self.task_participants_write.filter(id=task.id).exists()
        user_permissions['modify'] = user_permissions['modify'] or self.task_participants_modify.filter(id=task.id).exists()

        return user_permissions

@receiver(post_save, sender=User)
def update_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
    instance.profile.save()

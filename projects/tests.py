from django.test import RequestFactory, TestCase, Client
from user.models import Profile
from django.contrib.auth.models import AnonymousUser, User
from projects.models import Project, ProjectCategory, Task, TaskOffer
from projects.views import project_view


class TaskPermissionsTestCase(TestCase):
    def setup(self):
        category = ProjectCategory()
        category.save()

        owner_user = User(username='owner', password='user', email='user@user.com')
        owner_user.save()

        offerer_user = User(username='offerer', password='user', email='user@user.com')
        offerer_user.save()

        team_user = User(username='team', password='user', email='user@user.com')
        team_user.save()

        regular_user = User(username='regular', password='user', email='user@user.com')
        regular_user.save()

        owner_profile = Profile(user=owner_user, id=1)
        owner_profile.save()

        offerer_profile = Profile(user=offerer_user, id=2)
        offerer_profile.save()

        team_profile = Profile(user=team_user, id=3)
        team_profile.save()

        regular = Profile(user=regular_user, id=4)
        regular.save()

        project = Project(user=owner_profile, category=category)
        project.save()

        task = Task(project=project, id=1)
        # set team member permissions
        task.read.add(team_profile)
        task.write.add(team_profile)
        task.modify.add(team_profile)
        task.save()

        task_offer = TaskOffer(task=task, offerer=offerer_profile, status='a')
        task_offer.save()

    def test_get_user_task_permissions(self):
        owner = Profile.objects.get(id=1).user
        offerer = Profile.objects.get(id=2).user
        team_member = Profile.objects.get(id=3).user
        regular = Profile.objects.get(id=4).user
        task = Task.objects.get(id=1)

        # Test owner permissions
        owner_permissions_actual = owner.profile.get_user_task_permissions(task)
        owner_permissions_expected = {
            'write': True,
            'read': True,
            'modify': True,
            'owner': True,
            'upload': True,
        }
        self.assertEqual(owner_permissions_actual, owner_permissions_expected)

        # Test accepted offerer permissions
        offerer_permissions_actual = offerer.profile.get_user_task_permissions(task)
        offerer_permissions_expected = {
            'write': True,
            'read': True,
            'modify': True,
            'owner': False,
            'upload': True,
        }
        self.assertEqual(offerer_permissions_actual, offerer_permissions_expected)

        # Test team member permissions
        team_permissions_actual = team_member.profile.get_user_task_permissions(task)
        team_permissions_expected = {
            'write': True,
            'read': True,
            'modify': True,
            'owner': False,
            'view_task': False,
            'upload': False,
        }
        self.assertEqual(team_permissions_actual, team_permissions_expected)

        # Test regular user permissions, should not have any permissions
        regular_permissions_actual = regular.profile.get_user_task_permissions(task)
        regular_permissions_expected = {
            'write': False,
            'read': False,
            'modify': False,
            'owner': False,
            'view_task': False,
            'upload': False,
        }
        self.assertEqual(regular_permissions_actual, regular_permissions_expected)


class ProjectViewTestCase(TestCase):
    def setup(self):
        # create req factory
        self.factory = RequestFactory()

        # create user & anon user
        self.user = User.objects.create_user(username='user', password='user', email='user@user.com')
        self.anon_user = AnonymousUser()

        # create profile for user
        self.profile = Profile(user=self.user, id=1)
        self.profile.save()

        # create project category
        self.category = ProjectCategory()
        self.category.save()

        # create project
        self.project = Project(user=self.profile, category=self.category)
        self.project.save()

        # create task
        self.task = Task(project=self.project, id=1)
        self.task.save()

        # create task offer
        self.task_offer = TaskOffer(task=self.task, offerer=self.profile)
        self.task_offer.save()

    def test_project_view(self):
        # test offer response
        data = {'offer_response': '', 'taskofferid': 1, 'status': 'a', 'feedback': 'test'}
        request = self.factory.post('/projects', data)
        request.user = self.user
        response = project_view(request, 1)
        self.assertEqual(response.status_code, 200)

        # test status change
        data = {'status_change': '', 'status': self.project.status}
        request = self.factory.post('/projects', data)
        request.user = self.user
        response = project_view(request, 1)
        self.assertEqual(response.status_code, 200)

        # test offer submit response
        data = {'offer_submit': '', 'title': 'title', 'description': 'desc', 'price': 1, 'taskvalue': 1}
        request = self.factory.post('/projects', data)
        request.user = self.anon_user
        request.user.profile = self.profile
        response = project_view(request, 1)
        self.assertEqual(response.status_code, 200)

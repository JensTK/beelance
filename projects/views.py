from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404
from .models import Project, Task, TaskFile, TaskOffer, Delivery, ProjectCategory, Team, TaskFileTeam, Tag, directory_path
from .forms import ProjectForm, TaskFileForm, ProjectStatusForm, TaskOfferForm, TaskOfferResponseForm, TaskPermissionForm, DeliveryForm, TaskDeliveryResponseForm, TeamForm, TeamAddForm
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from user.models import Profile
from django.db.models import Q


def projects(request):
    # query exists
    if 'q' in request.GET:
        return query(request)
    # no query. Return all objects (may be sorted)
    else:
        return all_projects(request)


def date_sort(request, projects_to_be_sort):
    # sort projects (newest=desc, oldest=asc or default=none)
    projects_sorted = projects_to_be_sort
    sort = request.GET['date']
    if str(sort) == "asc":
        projects_sorted = projects_sorted.order_by('-date')
    elif str(sort) == "desc":
        projects_sorted = projects_sorted.order_by('date')
    return projects_sorted


def get_total_budget(tasks):
    budget = 0
    for task in tasks:
        budget += task.budget
    return budget


def query(request):
    q = request.GET['q']
    selected_tag = request.GET['tag']
    selected_price = request.GET['price']
    selected_sort = request.GET['date']

    # SEARCH: filter projects by title or desc containing query. non-sensitive
    criterion1 = Q(title__icontains=q)
    criterion2 = Q(description__icontains=q)
    projects_filtered = Project.objects.filter((criterion1 | criterion2))

    # TAG FILTER: filter projects for tag selected in dropdown menu
    tag_project_titles = []
    if request.GET['tag']:
        for project in projects_filtered:
            for t in project.tags.all():
                if str(t) == str(selected_tag):
                    tag_project_titles.append(project)
        if len(tag_project_titles) > 0:
            for title in tag_project_titles:
                criterion = Q(title__icontains=title)
                projects_filtered = projects_filtered.filter(criterion)
        else:
            projects_filtered = Project.objects.none()

    # BUDGET FILTER: filter projects for budget set
    for project in projects_filtered:
        budget = get_total_budget(project.tasks.all())
        if int(selected_price) > int(budget):
            projects_filtered = projects_filtered.exclude(id=project.id)

    # DATE SORT sort projects (newest=desc, oldest=asc or default=none):
    if request.GET['date']:
        projects_filtered = date_sort(request, projects_filtered)

    return render(request,
                  'projects/projects.html',
                  {
                      'projects': projects_filtered,
                      'project_categories': ProjectCategory.objects.all(),
                      'query': q,
                      'tags': Tag.objects.all(),
                      'selected_tag': selected_tag,
                      'selected_price': int(selected_price),
                      'selected_sort': selected_sort
                  }
                  )


def all_projects(request):
    projects_all = Project.objects.all()
    sort = ""
    if 'date' in request.GET and request.GET['date']:
        sort = request.GET['date']
        projects_all = date_sort(request, projects_all)

    return render(request,
                  'projects/projects.html',
                  {
                      'projects': projects_all,
                      'project_categories': ProjectCategory.objects.all(),
                      'tags': Tag.objects.all(),
                      'selected_tag': "",
                      'selected_price': int(0),
                      'selected_sort': sort
                  }
                  )


@login_required
def new_project(request):
    from django.contrib.sites.shortcuts import get_current_site
    current_site = get_current_site(request)
    if request.method == 'POST':
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(commit=False)

            project.user = request.user.profile
            project.save()
            for tag in form.cleaned_data['tags'].all():
                project.tags.add(tag)
            project.save()

            people = Profile.objects.filter(categories__id=project.category.id)
            from django.core import mail
            for person in people:
                if person.user.email:
                    try:
                        with mail.get_connection() as connection:
                            mail.EmailMessage(
                                "New Project: " + project.title , "A new project you might be interested in was created and can be viwed at " + current_site.domain + '/projects/' + str(project.id), "Beelancer", [person.user.email],
                                connection=connection,
                            ).send()
                    except Exception as e:
                        from django.contrib import messages
                        messages.success(request, 'Sending of email to ' + person.user.email + " failed: " + str(e))

            task_title = request.POST.getlist('task_title')
            task_description = request.POST.getlist('task_description')
            task_budget = request.POST.getlist('task_budget')
            for i in range(0, len(task_title)):
                Task.objects.create(
                    title = task_title[i],
                    description = task_description[i],
                    budget = task_budget[i],
                    project = project,
                )
            return redirect('project_view', project_id=project.id)
    else:
        form = ProjectForm()
    return render(request, 'projects/new_project.html', {'form': form})


def project_view(request, project_id):
    project = Project.objects.get(pk=project_id)
    tasks = project.tasks.all()
    total_budget = get_total_budget(tasks)

    if request.user == project.user.user:

        if request.method == 'POST' and 'offer_response' in request.POST:
            instance = get_object_or_404(TaskOffer, id=request.POST.get('taskofferid'))
            offer_response_form = TaskOfferResponseForm(request.POST, instance=instance)
            if offer_response_form.is_valid():
                offer_response = offer_response_form.save(commit=False)

                if offer_response.status == 'a':
                    offer_response.task.read.add(offer_response.offerer)
                    offer_response.task.write.add(offer_response.offerer)
                    project = offer_response.task.project
                    project.participants.add(offer_response.offerer)

                offer_response.save()
        offer_response_form = TaskOfferResponseForm()

        if request.method == 'POST' and 'status_change' in request.POST:
            status_form = ProjectStatusForm(request.POST)
            if status_form.is_valid():
                project_status = status_form.save(commit=False)
                project.status = project_status.status
                project.save()
        status_form = ProjectStatusForm(initial={'status': project.status})

        return render(request, 'projects/project_view.html', {
            'project': project,
            'tasks': tasks,
            'status_form': status_form,
            'total_budget': total_budget,
            'offer_response_form': offer_response_form,
        })

    else:
        if request.method == 'POST' and 'offer_submit' in request.POST:
            task_offer_form = TaskOfferForm(request.POST)
            if task_offer_form.is_valid():
                task_offer = task_offer_form.save(commit=False)
                task_offer.task =  Task.objects.get(pk=request.POST.get('taskvalue'))
                task_offer.offerer = request.user.profile
                task_offer.save()
        task_offer_form = TaskOfferForm()

        return render(request, 'projects/project_view.html', {
            'project': project,
            'tasks': tasks,
            'task_offer_form': task_offer_form,
            'total_budget': total_budget,
        })


@login_required
def upload_file_to_task(request, project_id, task_id):
    project = Project.objects.get(pk=project_id)
    task = Task.objects.get(pk=task_id)
    user_permissions = request.user.profile.get_user_task_permissions(task)
    print(user_permissions)
    print(request.method)
    accepted_task_offer = task.accepted_task_offer()

    permission = user_permissions['modify'] or user_permissions['write'] or user_permissions['upload'] or \
        project.is_project_owner(request.user)

    if permission and request.method == 'POST':
        task_file_form = TaskFileForm(request.POST, request.FILES)
        if task_file_form.is_valid():
            task_file = task_file_form.save(commit=False)
            task_file.task = task
            existing_file = task.files.filter(file=directory_path(task_file, task_file.file.file)).first()

            access = False
            for t in request.user.profile.teams.all():
                if TaskFileTeam.objects.filter(team=t, file=existing_file, modify=True).exists():
                    access = True
                    break

            if user_permissions['modify'] or user_permissions['owner'] or access:
                if existing_file:
                    existing_file.delete()
                task_file.save()

                if request.user.profile != project.user and request.user.profile != accepted_task_offer.offerer:
                    teams = request.user.profile.teams.filter(task__id=task.id)
                    for team in teams:
                        tft = TaskFileTeam()
                        tft.team = team
                        tft.file = task_file
                        tft.read = True
                        tft.save()
            else:
                from django.contrib import messages
                messages.warning(request, "You do not have access to modify this file")

            return redirect('task_view', project_id=project_id, task_id=task_id)

    if permission and request.method == "GET":
        return render(
            request,
            'projects/upload_file_to_task.html',
            {
                'project': project,
                'task': task,
                'task_file_form': TaskFileForm(),
            }
        )

    return redirect('/user/login')


def delivery(request, task_id):
    user = request.user
    task = Task.objects.get(pk=task_id)
    deliver_form = DeliveryForm(request.POST, request.FILES)
    if deliver_form.is_valid():
        delivery = deliver_form.save(commit=False)
        delivery.task = task
        delivery.delivery_user = user.profile
        delivery.save()
        task.status = "pa"
        task.save()


def delivery_response(request, task_id):
    user = request.user
    task = Task.objects.get(pk=task_id)

    instance = get_object_or_404(Delivery, id=request.POST.get('delivery-id'))
    deliver_response_form = TaskDeliveryResponseForm(request.POST, instance=instance)
    if deliver_response_form.is_valid():
        delivery = deliver_response_form.save()
        from django.utils import timezone
        delivery.responding_time = timezone.now()
        delivery.responding_user = user.profile
        delivery.save()

        if delivery.status == 'a':
            task.status = "pp"
            task.save()
        elif delivery.status == 'd':
            task.status = "dd"
            task.save()


def team(request, task_id):
    task = Task.objects.get(pk=task_id)

    team_form = TeamForm(request.POST)
    if team_form.is_valid():
        team = team_form.save(False)
        team.task = task
        team.save()


def team_add(request):
    instance = get_object_or_404(Team, id=request.POST.get('team-id'))
    team_add_form = TeamAddForm(request.POST, instance=instance)
    if team_add_form.is_valid():
        team = team_add_form.save(False)
        team.members.add(*team_add_form.cleaned_data['members'])
        team.save()


def permissions(request, task_id):
    task = Task.objects.get(pk=task_id)

    for t in task.teams.all():
        for f in task.files.all():
            try:
                tft_string = 'permission-perobj-' + str(f.id) + '-' + str(t.id)
                tft_id = request.POST.get(tft_string)
                instance = TaskFileTeam.objects.get(id=tft_id)
            except Exception as e:
                instance = TaskFileTeam(
                    file=f,
                    team=t,
                )

            instance.read = request.POST.get('permission-read-' + str(f.id) + '-' + str(t.id))  or False
            instance.write = request.POST.get('permission-write-' + str(f.id) + '-' + str(t.id)) or False
            instance.modify = request.POST.get('permission-modify-' + str(f.id) + '-' + str(t.id))  or False
            instance.save()
        t.write = request.POST.get('permission-upload-' + str(t.id)) or False
        t.save()


@login_required
def task_view(request, project_id, task_id):
    user = request.user
    task = Task.objects.get(pk=task_id)
    project = Project.objects.get(pk=project_id)
    accepted_task_offer = task.accepted_task_offer()

    user_permissions = request.user.profile.get_user_task_permissions(task)
    if not user_permissions['read'] and not user_permissions['write'] and not user_permissions['modify'] and \
            not user_permissions['owner'] and not user_permissions['view_task']:
        return redirect('/user/login')

    if request.method == 'POST':
        if 'delivery' in request.POST and accepted_task_offer and accepted_task_offer.offerer == user.profile:
            delivery(request, task_id)

        if 'delivery-response' in request.POST:
            delivery_response(request, task_id)

        if 'team' in request.POST and accepted_task_offer \
                and accepted_task_offer.offerer == user.profile:
            team(request, task_id)

        if 'team-add' in request.POST and accepted_task_offer \
                and accepted_task_offer.offerer == user.profile:
            team_add(request)

        if 'permissions' in request.POST and accepted_task_offer \
                and accepted_task_offer.offerer == user.profile:
            permissions(request, task_id)

    if user_permissions['read'] or user_permissions['write'] or user_permissions['modify'] or user_permissions['owner']\
            or user_permissions['view_task']:
        deliveries = task.delivery.all()
        team_files = []
        per = {}
        for f in task.files.all():
            per[f.name()] = {}
            for p in f.teams.all():
                per[f.name()][p.team.name] = p
                if p.read:
                    team_files.append(p)
        return render(request, 'projects/task_view.html', {
                'task': task,
                'project': project,
                'user_permissions': user_permissions,
                'deliver_form': DeliveryForm(),
                'deliveries': deliveries,
                'deliver_response_form': TaskDeliveryResponseForm(),
                'team_form': TeamForm(),
                'team_add_form': TeamAddForm(),
                'team_files': team_files,
                'per': per
                })

    return redirect('/user/login')


@login_required
def task_permissions(request, project_id, task_id):
    user = request.user
    task = Task.objects.get(pk=task_id)
    project = Project.objects.get(pk=project_id)
    accepted_task_offer = task.accepted_task_offer()
    if (project.user == request.user.profile or user == accepted_task_offer.offerer.user) \
            and int(project_id) == task.project.id:
        if request.method == 'POST':
            task_permission_form = TaskPermissionForm(request.POST)
            if task_permission_form.is_valid():
                try:
                    username = task_permission_form.cleaned_data['user']
                    user = User.objects.get(username=username)
                    permission_type = task_permission_form.cleaned_data['permission']
                    if permission_type == 'Read':
                        task.read.add(user.profile)
                    elif permission_type == 'Write':
                        task.write.add(user.profile)
                    elif permission_type == 'Modify':
                        task.modify.add(user.profile)
                except Exception:
                    print("user not found")
                return redirect('task_view', project_id=project_id, task_id=task_id)

        return render(
            request,
            'projects/task_permissions.html',
            {
                'project': project,
                'task': task,
                'form': TaskPermissionForm(),
            }
        )
    return redirect('task_view', project_id=project_id, task_id=task_id)


@login_required
def delete_file(request, file_id):
    f = TaskFile.objects.get(pk=file_id)
    f.delete()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

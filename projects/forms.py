from django import forms
from .models import Project, TaskFile, TaskOffer, Delivery, Team
from django.contrib.auth.models import User


class ProjectForm(forms.ModelForm):
    class Meta:
        model = Project
        fields = ('title', 'description', 'category', 'tags')


class TaskFileForm(forms.ModelForm):
    class Meta:
        model = TaskFile
        fields = ('file',)


class ProjectStatusForm(forms.ModelForm):
    class Meta:
        model = Project
        fields = ('status',)


class TaskOfferForm(forms.ModelForm):
    class Meta:
        model = TaskOffer
        fields = ('title', 'description', 'price',)


class TaskOfferResponseForm(forms.ModelForm):
    class Meta:
        model = TaskOffer
        fields = ('status', 'feedback')


class TaskDeliveryResponseForm(forms.ModelForm):
    class Meta:
        model = Delivery
        fields = ('status', 'feedback')


PERMISSION_CHOICES = (
    ('Read','Read'),
    ('Write', 'Write'),
    ('Modify','Modify'),
)


class TaskPermissionForm(forms.Form):
    user = forms.ModelChoiceField(queryset=User.objects.all())
    permission = forms.ChoiceField(choices=PERMISSION_CHOICES)


class DeliveryForm(forms.ModelForm):
    class Meta:
        model = Delivery
        fields = ('comment', 'file')


class TeamForm(forms.ModelForm):
    class Meta:
        model = Team
        fields = ('name',)


class TeamAddForm(forms.ModelForm):
    class Meta:
        model = Team
        fields = ('members', )
